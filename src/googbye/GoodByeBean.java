package googbye;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

@ManagedBean(name = "goodbyeBean")
@ViewScoped
public class GoodByeBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	int number;

	@PostConstruct
	public void init() {
		
		 number = (int) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("number");
		
	}


	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}



}
