package connection;


import java.io.Serializable;
import java.sql.Connection;

import javax.naming.InitialContext;
import javax.sql.DataSource;

public class ServiceLocator  implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static Connection getConexao(String JNDINome) throws Exception {
		Connection con = null;
		InitialContext contexto = new InitialContext();

		DataSource ds = (DataSource) contexto.lookup("java:comp/env/"
				+ JNDINome);

		con = ds.getConnection();

		return con;
	}
}