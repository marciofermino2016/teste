package number;


import java.io.IOException;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.primefaces.event.SelectEvent;

import com.mysql.jdbc.Statement;

import connection.ServiceLocator;

@ManagedBean(name = "numberBean")
@ViewScoped

public class NumberBean  implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final String JNDINome = "jdbc/teste";
	
	
	private List<NumberBean> listNumber;
	int id;
	int number;
	
	
	
	public List<NumberBean> getListNumber() {
		return listNumber;
	}
	public void setListNumber(List<NumberBean> listNumber) {
		this.listNumber = listNumber;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getNumber() {
		return number;
	}
	public void setNumber(int number) {
		this.number = number;
	}
	
	
	public void insert(){
		
		
		Connection con = null;
		PreparedStatement comando = null;
		Statement stmt = null;
		
		
		
		
		try {
			
			String SQL = null;
			
			con = ServiceLocator.getConexao(JNDINome);
			
						
			for (int i = 0;  i<number; i++) {
				
				SQL = "INSERT INTO number (number) ";
				
				SQL = SQL + "VALUES (";
				
				SQL = SQL + i;
				
				SQL = SQL +")";

				
				comando = con.prepareStatement(SQL);
				comando.execute();		
				
			
			
			}
		
			comando.close();
			con.close();
		
			consult();
		
		} catch (Exception e) {
			System.out.println("erro ao gerar numeros");
		}
		
		
		
	}
	
	 public ArrayList<NumberBean> consult() throws Exception {

	        Connection con = null;
	        PreparedStatement comando = null;
	        ResultSet rs = null;
	        
	        
	        try {

	            con = ServiceLocator.getConexao(JNDINome);
	            String SQL = "SELECT * FROM number";
	            comando = (PreparedStatement) con.prepareStatement(SQL);
	            rs = (ResultSet) comando.executeQuery();
	            ArrayList<NumberBean> lst = new ArrayList<NumberBean>();
	            
	            
	            while (rs.next()) {

	            	NumberBean numberBean = new NumberBean();

	            	numberBean.setId(rs.getInt("idnumber"));
	            	numberBean.setNumber(rs.getInt("number"));
	            		            
	                lst.add(numberBean);
	            }
	            

				
				
	            rs.close();
	            comando.close();
	            con.close();

	            listNumber = lst;

	        	FacesContext.getCurrentInstance().getExternalContext()
				.getSessionMap().put("number",number);
	        	
	        	
	            return lst;
	        } catch (SQLException ex) {
	            ex.printStackTrace();
	            return null;
	        }
	    }


	 
	 public void googbye() {
	
		 try {
				FacesContext.getCurrentInstance().getExternalContext().redirect("/teste/goodbye.xhtml");
		 } catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
		 }
		 
	 }
	 
	 public void onRowNumber(SelectEvent event) {

	        setId(((NumberBean) event.getObject()).getId());
	        setNumber(((NumberBean) event.getObject()).getNumber());
	        
	        
	  }
	    

	
}
